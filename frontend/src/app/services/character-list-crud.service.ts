import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";

import { ErrorHandlerService } from "./error-handler.service";

import { Character } from "../models/character";

@Injectable({
  providedIn: "root",
})
export class CharacterListCrudService {
  private url = "http://localhost:8080/characters";

  httpOptions: { headers: HttpHeaders } = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };

  constructor(
    private errorHandlerService: ErrorHandlerService,
    private http: HttpClient
  ) {}

  fetchAll(): Observable<Character[]> {
    return this.http
      .get<Character[]>(this.url, { responseType: "json" })
    }

  post(item: Partial<Character>): Observable<any> {
    return this.http
      .post<Partial<Character>>(this.url, item, this.httpOptions)
  }


  delete(id: number): Observable<any> {
    const url = `http://localhost:8080/characters/${id}`;

    return this.http
      .delete<Character>(url, this.httpOptions)
  }
}