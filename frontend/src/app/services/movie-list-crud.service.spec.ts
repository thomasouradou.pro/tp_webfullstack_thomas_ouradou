import { TestBed } from '@angular/core/testing';

import { MovieListCrudService } from './movie-list-crud.service';

describe('MovieListCrudService', () => {
  let service: MovieListCrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovieListCrudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
