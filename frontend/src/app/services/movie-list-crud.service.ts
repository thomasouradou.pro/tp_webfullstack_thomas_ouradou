import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";

import { ErrorHandlerService } from "./error-handler.service";

import { Movie } from "../models/movie";

@Injectable({
  providedIn: "root",
})
export class MovieListCrudService {
  private url = "http://localhost:8080/movies";

  httpOptions: { headers: HttpHeaders } = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };

  constructor(
    private errorHandlerService: ErrorHandlerService,
    private http: HttpClient
  ) {}

  fetchAll(): Observable<Movie[]> {
    return this.http
      .get<Movie[]>(this.url, { responseType: "json" })
    }

  post(item: Partial<Movie>): Observable<any> {
    return this.http
      .post<Partial<Movie>>(this.url, item, this.httpOptions)
  }


  delete(id: number): Observable<any> {
    const url = `http://localhost:8080/movies/${id}`;

    return this.http
      .delete<Movie>(url, this.httpOptions)
  }
}