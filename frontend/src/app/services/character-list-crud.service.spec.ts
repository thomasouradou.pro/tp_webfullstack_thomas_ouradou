import { TestBed } from '@angular/core/testing';

import { CharacterListCrudService } from './character-list-crud.service';

describe('CharacterListCrudService', () => {
  let service: CharacterListCrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharacterListCrudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
