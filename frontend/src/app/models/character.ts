export class Character {
    id : number;
    first_name : string;
    last_name : string;
    birth_date : Date;
}
