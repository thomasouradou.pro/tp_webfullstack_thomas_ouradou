export class Movie {
    id : number;
    name : string;
    author : string;
    release_date : Date;
}
