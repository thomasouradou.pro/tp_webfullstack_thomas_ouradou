import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { MovieListCrudService } from "../../services/movie-list-crud.service";

import { Movie } from "src/app/models/Movie";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-movie-list",
  templateUrl: "./movie-list.component.html",
  styleUrls: ["./movie-list.component.css"],
})
export class MovieListComponent implements OnInit {
  movies$
name
author
releaseDate
  constructor(private movieListCrudService: MovieListCrudService) {}

  ngOnInit(): void {
    this.fetchAll();
  }

  fetchAll(){
    return this.movieListCrudService.fetchAll().subscribe(res=>{
      this.movies$ = res,
      console.log(res)
  });
}

  post(movieItem: Partial<Movie>): void {
   if (movieItem.name === "" || movieItem.author === "")
      return;

console.log(movieItem.name, movieItem.author, movieItem.release_date)
  this.movieListCrudService.post({ name : movieItem.name, author : movieItem.author, release_date :movieItem.release_date}).subscribe(res=>{
    this.fetchAll()
  })
  }


  delete(id: number): void {
    this.movieListCrudService.delete(id).subscribe(res=>{
      this.fetchAll()
    })
  }
}