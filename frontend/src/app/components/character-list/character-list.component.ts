import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { CharacterListCrudService } from "../../services/character-list-crud.service";

import { Character } from "src/app/models/Character";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-character-list",
  templateUrl: "./character-list.component.html",
  styleUrls: ["./character-list.component.css"],
})
export class CharacterListComponent implements OnInit {
  characters$
firstName
lastName
birthDate
  constructor(private characterListCrudService: CharacterListCrudService) {}

  ngOnInit(): void {
    this.fetchAll();
  }

  fetchAll(){
    return this.characterListCrudService.fetchAll().subscribe(res=>{
      this.characters$ = res,
      console.log(res)
  });
}

  post(characterItem: Partial<Character>): void {
   if (characterItem.first_name === "" || characterItem.last_name === "")
      return;

console.log(characterItem.first_name, characterItem.last_name, characterItem.birth_date)
  this.characterListCrudService.post({ first_name : characterItem.first_name, last_name : characterItem.last_name, birth_date :characterItem.birth_date}).subscribe(res=>{
    this.fetchAll()
  })
  }


  delete(id: number): void {
    this.characterListCrudService.delete(id).subscribe(res=>{
      this.fetchAll()
    })
  }
}