const express = require('express');

const characterController = require('../controllers/character');

const router = express.Router();

router.get('/', characterController.getAllCharacters);

router.post('/', characterController.postCharacter);

router.delete('/:id', characterController.deleteCharacter);

module.exports = router;