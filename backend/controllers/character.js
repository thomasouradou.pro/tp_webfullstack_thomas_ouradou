const Character = require('../models/character');

exports.getAllCharacters = async(req, res, next) => {
    try {
        const [allCharacters] = await Character.fetchAll();
        res.status(200).json(allCharacters);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.postCharacter = async(req, res, next) => {
    try {
        const postResponse = await Character.post(req.body.first_name, req.body.last_name, req.body.birth_date);
        res.status(201).json(postResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};



exports.deleteCharacter = async(req, res, next) => {
    try {
        const deleteResponse = await Character.delete(req.params.id);
        res.status(200).json(deleteResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};